#include <stdlib.h>
#include <unistd.h>

int main() {
    int A;

    #pragma oss task weakin(A)
    {
        sleep(1);
    }
    #pragma oss task weakout(A)
    {
    }
    #pragma oss task in(A)
    {
    }
}
