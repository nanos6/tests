#include <stdlib.h>
#include <unistd.h>

int main() {
    int A;

    #pragma oss task inout(A)
    {
        sleep(1);
    }
    
    #pragma oss task weakin(A)
    {
        #pragma oss task weakin(A)
        {
            #pragma oss task in(A)
            {
            }
        }
    }
}
