#include <stdlib.h>
#include <unistd.h>

int main() {
    int A;

    #pragma oss task weakout(A)
    {
        #pragma oss task out(A)
        {
            #pragma oss task out(A)
            {
            }
        }
        sleep(1);
    }
    #pragma oss task weakinout(A)
    {
        #pragma oss task weakin(A)
        {
        }
    }
}
