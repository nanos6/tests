int main() {
    int A;

    #pragma oss task weakinout(A)
    {
        #pragma oss task weakin(A)
        {
            #pragma oss task weakin(A)
            {
            }
        }
        #pragma oss task weakin(A)
        {
            #pragma oss task weakin(A)
            {
            }
            #pragma oss task weakin(A)
            {
            }
            #pragma oss task weakin(A)
            {
            }
            #pragma oss task in(A)
            {
            }
        }
    }
}
