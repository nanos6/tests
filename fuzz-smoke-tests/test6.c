int main() {
    int A;

    #pragma oss task weakin(A)
    {
        #pragma oss task weakin(A)
        {
            #pragma oss task weakin(A)
            {
            }
            #pragma oss task weakin(A)
            {
            }
            #pragma oss task weakin(A)
            {
            }
        }
        #pragma oss task weakin(A)
        {
            #pragma oss task in(A)
            {
            }
        }
        #pragma oss task in(A)
        {
            #pragma oss task in(A)
            {
            }
            #pragma oss task in(A)
            {
            }
            #pragma oss task in(A)
            {
            }
            #pragma oss task in(A)
            {
            }
        }
    }
    #pragma oss task weakout(A)
    {
    }
    #pragma oss task weakout(A)
    {
        #pragma oss task weakout(A)
        {
            #pragma oss task out(A)
            {
            }
            #pragma oss task weakout(A)
            {
            }
            #pragma oss task weakout(A)
            {
            }
            #pragma oss task weakout(A)
            {
            }
        }
        #pragma oss task weakout(A)
        {
            #pragma oss task weakout(A)
            {
            }
        }
    }
}
