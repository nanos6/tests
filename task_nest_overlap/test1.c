/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../test.h"

int a, b = 0;

int main(int argc, char *argv[])
{
	#pragma oss task inout(a, b)
	{
		#pragma oss task inout(a)
		{
			a = 1;
		}

		#pragma oss task inout(b)
		{
			b = 1;
		}

		// #pragma oss taskwait
	}

	#pragma oss taskwait

	test_result("task_nest_overlap", 2, (a == 1 && b == 1));

	return 0;
}
