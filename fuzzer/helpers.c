#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "helpers.h"
#include "fuzzer.h"

const char * type_list[] = { "in", "out", "inout", "concurrent", "commutative" };
const char * strong_list[] = { "weak", "" };

char desc_buffer[100];

int has_children(Task *task) {
    return (task && task->children.num_tasks > 0);
}

const char * get_task_description(Task * t) {
    sprintf(desc_buffer, "%s%s", strong_list[t->task_weakness], type_list[t->task_type]);
    return desc_buffer;
}

void die(const char * reason) {
    fprintf(stderr, "%s\n", reason);
    exit(1);
}
