/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "fuzzer.h"
#include "helpers.h"

Task * current_base;
time_t time_seed = 0L;

int main(int argc, char *argv[])
{
    initialize_fuzzer();
    int num_runs = FUZZ_RUNS;

    if(argc >= 2)
        num_runs = atoi(argv[1]);

    if(argc >= 3)
        time_seed = atoi(argv[2]);

    for(int i = 0; i < num_runs; ++i) {
        printf("Run %d/%d | ", i, num_runs);
        current_base = get_sequence();
        printf("Sequence Done | ");
        process_next(current_base);
        printf("Tasks Created | ");
        #pragma oss taskwait
        printf("Tasks Finished | ");
        free_sequence(current_base);
        printf("Cleaned Up\r");
    }

    #pragma oss taskwait

    printf("\nDone\n");
}

DEF_ACC(in)
DEF_ACC(out)
DEF_ACC(inout)
DEF_ACC(concurrent)
DEF_ACC(commutative)
DEF_ACC(weakin)
DEF_ACC(weakout)
DEF_ACC(weakinout)
DEF_ACC(weakcommutative)

void process_next(Task * this) {
    if(this && has_children(this)) {
        TaskCollection children = this->children;
        Task * child;
        for(int i = 0; i < children.num_tasks; ++i) {
            child = &children.tasks[i];

            if(child->task_type == READ)
                if(child->task_weakness == WEAK)
                    ompss_invoke_weakin_task(child);
                else
                    ompss_invoke_in_task(child);
            else if(child->task_type == WRITE)
                if(child->task_weakness == WEAK)
                    ompss_invoke_weakout_task(child);
                else
                    ompss_invoke_out_task(child);
            else if(child->task_type == READWRITE)
                if(child->task_weakness == WEAK)
                    ompss_invoke_weakinout_task(child);
                else
                    ompss_invoke_inout_task(child);
            else if(child->task_type == CONCURRENT)
                ompss_invoke_concurrent_task(child);
            else
                if(child->task_weakness == WEAK)
                    ompss_invoke_weakcommutative_task(child);
                else
                    ompss_invoke_commutative_task(child);
        }
    }
}

Task * get_sequence() {
    Task * t = (Task *) malloc(sizeof(Task));

    if(!t)
        die("Out of memory");

    populate_collection(&t->children, 1, WEAK, READWRITE);

    return t;
}

void free_taskcollection(TaskCollection * collection) {
    for(int i = 0; i < collection->num_tasks; ++i)
        free_taskcollection(&collection->tasks[i].children);

    if(collection->num_tasks > 0)
        free(collection->tasks);
}

void free_sequence(Task * task) {
    if(task->children.num_tasks > 0)
        free_taskcollection(&task->children);

    free(task);
}


void print_task(Task * task, int level, FILE * handle) {
    fprintf(handle, "%*s#pragma oss task %s(A)\n", level * 4, "", get_task_description(task));
    fprintf(handle, "%*s{\n", level * 4, "");

    for(int i = 0; i < task->children.num_tasks; ++i)
        print_task(&task->children.tasks[i], level + 1, handle);

    fprintf(handle, "%*s}\n", level * 4, "");
}

void print_sequence(Task * task) {
    FILE * handle = fopen("reproducer.c", "w");
    fprintf(handle, "int main() {\n");
    fprintf(handle, "    int A;\n\n");
    for(int i = 0; i < task->children.num_tasks; ++i)
        print_task(&task->children.tasks[i], 1, handle);
    fprintf(handle, "    #pragma oss taskwait\n");
    fprintf(handle, "}\n");
    fclose(handle);
}

void populate_collection(TaskCollection * collection, int level, TaskWeakness currentTaskWeakness, TaskType currentTaskType) {
    int last_level = (level == MAX_NEST);
    int tasks = rand() % (MAX_TASKS_LEVEL + 1);
    collection->num_tasks = tasks;

    if(tasks) {
        collection->tasks = malloc(sizeof(Task) * tasks);
        if(!collection->tasks)
            die("Out of memory");
    }

    for(int i = 0; i < tasks; ++i) {
        Task * task = &collection->tasks[i];
        task->task_type = get_task_type(currentTaskType);

        if(task->task_type == CONCURRENT)
            task->task_weakness = STRONG;
        else
            task->task_weakness = get_task_strongness(currentTaskWeakness);

        if(last_level)
            task->children.num_tasks = 0;
        else
            populate_collection(&task->children, level + 1, task->task_weakness, task->task_type);
    }
}

void debug_handler(int signo, siginfo_t * info, void * extra) {
    if(signo == SIGSEGV)
        printf("\nSegfaulted. Dumping program.\n");
    else if (signo == SIGABRT)
        printf("\nAborted. Dumping program.\n");
    else
        printf("\nInterrupted. Dumping program.\n");

    print_sequence(current_base);

    exit(1);
}

void initialize_fuzzer() {
    if(time_seed == 0L)
        time_seed = time(NULL);
    srand(time_seed);
    printf("Seed: %ld\n", time_seed);
    setvbuf(stdout, NULL, _IONBF, 0);

    struct sigaction action;
    action.sa_flags = SA_SIGINFO;
    action.sa_sigaction = &debug_handler;
    sigaction(SIGSEGV, &action, NULL);
    sigaction(SIGABRT, &action, NULL);
    sigaction(SIGINT, &action, NULL);
}
