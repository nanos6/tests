#ifndef HELPERS_H
#define HELPERS_H

#include "fuzzer.h"
#include <stdlib.h>

int has_children(Task * task);

__attribute__((always_inline)) inline TaskType get_task_type(TaskType currentTaskType) {
    if(currentTaskType == READWRITE)
        return (TaskType) (rand() % 5);
    return currentTaskType;
}

__attribute__((always_inline)) inline TaskWeakness get_task_strongness(TaskWeakness currentTaskWeakness) {
    if(currentTaskWeakness == WEAK)
        return (TaskWeakness) (rand() % 2);
    return STRONG;
}

const char * get_task_description(Task * t);

void die(const char * reason);


#endif // HELPERS_H
