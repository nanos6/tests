#ifndef FUZZER_H
#define FUZZER_H

#include <signal.h>
#include <sys/types.h>

#define MAX_TASKS_LEVEL 5
#define MAX_NEST 3
#define FUZZ_RUNS 1

int _depend_var;

#define STRINGIFY(a) #a
#define DEF_ACC(construct) _Pragma(STRINGIFY(oss task construct(_depend_var))) \
    void ompss_invoke_## construct ##_task(Task * this) { process_next(this); }

typedef enum _TaskType {
    READ = 0,
    WRITE = 1,
    READWRITE = 2,
    CONCURRENT = 3,
    COMMUTATIVE = 4
} TaskType;

typedef enum _TaskWeakness {
    WEAK = 0,
    STRONG = 1
} TaskWeakness;

struct _Task;

typedef struct _TaskCollection {
    int num_tasks;
    struct _Task * tasks;
} TaskCollection;

typedef struct _Task {
    TaskType task_type;
    TaskWeakness task_weakness;
    TaskCollection children;
} Task;

void process_next(Task * this);
Task * get_sequence();
void free_sequence(Task * base);
void populate_collection(TaskCollection * collection, int level, TaskWeakness currentTaskWeakness, TaskType currentTaskType);
void initialize_fuzzer();
void print_sequence(Task * base);
void debug_handler(int signo, siginfo_t * info, void * extra);

#endif // FUZZER_H
