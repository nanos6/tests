/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
	

  #pragma oss task weakreduction(+: a)
  {
    #pragma oss task reduction(+: a)
    a++;

    #pragma oss taskwait

    #pragma oss task reduction(+: a)
    a++;
  }

  #pragma oss task weakreduction(+: a)
  {
    #pragma oss task reduction(+: a)
    a++;
  }

#pragma oss taskwait
#pragma oss task in(a)
  {
	  test_result("weakreduction", 2, a == 3);
  }

#pragma oss taskwait

	return 0;
}
