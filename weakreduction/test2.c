/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  int a = 0;
	

    #pragma oss task weakreduction(+: a)
	{
        #pragma oss task weakreduction(+: a)
        {
	        #pragma oss task reduction(+: a)
            {
		        a++;
		    }
            #pragma oss taskwait
        }
        
        sleep(1);

        #pragma oss taskwait
	}


#pragma oss taskwait
#pragma oss task in(a)
  {
      if (a != 1) {
        printf("Test failed\n");
      }
  }

#pragma oss taskwait

	return 0;
}
