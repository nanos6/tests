/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../test.h"

#define LENGTH 1
#define TIMES 2

void sum_array(int *arr, size_t length)
{
	for (size_t i = 0; i < length; ++i)
	{
		arr[i]++;
	}
}

int *init_int_arr(size_t length)
{
	int *res = malloc(length * sizeof(int));
	memset(res, 0, length * sizeof(int));

	return res;
}

int check_array_is(int *arr, size_t length, int val)
{
	for (size_t i = 0; i < length; ++i)
	{
		if (arr[i] != val)
		{
			printf("Err: arr[%lu] (%d) != %d\n", i, arr[i], val);
			return 0;
		}
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int len = LENGTH;

	int *arr1 = init_int_arr(len);

	for (int i = 0; i < TIMES; ++i)
	{
		#pragma oss task inout(arr1 [0:len - 1])
		sum_array(arr1, len);
	}

	long long int res = 0;

	for (int i = 0; i < TIMES; ++i)
	{
		#pragma oss task in(arr1 [0:len - 1]) reduction(+: res)
		for (size_t i = 0; i < len; ++i)
		{
			res += arr1[i];
		}

		for (volatile int j = 0; j < 100000000; ++j);
	}

	#pragma oss taskwait
	#pragma oss task in(res)
	{
		printf("Result: %lld\n", res);
		test_result("reduction_deadlock", 1, res == 4);
	}

	#pragma oss taskwait

	free(arr1);

	return 0;
}
