/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;

  #pragma oss task inout(a)
  {
    #pragma oss task reduction(+: a)
    a++;
  }

  #pragma oss task in(a)
  {
    printf("Res: %d\n", a);
    test_result("reduction_inout", 1, a == 1);
  }

#pragma oss taskwait

	return 0;
}
