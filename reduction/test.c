/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../test.h"

#define LENGTH 1024
#define TIMES 100000

void sum_array(int* arr, size_t length) {
	for(size_t i = 0; i < length; ++i) {
		arr[i]++;
	}
}

int * init_int_arr(size_t length) {
	int * res = malloc(length * sizeof(int));
	memset(res, 0, length * sizeof(int));

	return res;
}

int check_array_is(int* arr, size_t length, int val) {
	for(size_t i = 0; i < length; ++i) {
		if(arr[i] != val) {
      printf("Err: arr[%lu] (%d) != %d\n", i, arr[i], val);
      return 0;
    }
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int len = LENGTH;

	int* arr1 = init_int_arr(LENGTH);
	
	for(int i = 0; i < TIMES; ++i) {
		#pragma oss task inout(arr1 [0:len])
    	sum_array(arr1, LENGTH);
	}

  long long int res = 0;
  
  for(size_t i = 0; i < LENGTH; ++i) {
    #pragma oss task in(arr1[0:len]) reduction(+:res)
    {
      res += arr1[i];
    }
  }

#pragma oss taskwait
#pragma oss task in(res)
  {
	  test_result("reduction", 1, res == 102400000);
  }

#pragma oss taskwait

	free(arr1);

	return 0;
}
