/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../test.h"

#define LENGTH 1
#define TIMES 1

void sum_array(int* arr, size_t length) {
	for(size_t i = 0; i < length; ++i) {
		arr[i]++;
	}
}

int * init_int_arr(size_t length) {
	int * res = malloc(length * sizeof(int));
	memset(res, 0, length * sizeof(int));

	return res;
}

int check_array_is(int* arr, size_t length, int val) {
	for(size_t i = 0; i < length; ++i) {
		if(arr[i] != val) {
      printf("Err: arr[%lu] (%d) != %d\n", i, arr[i], val);
      return 0;
    }
	}

	return 1;
}

void busy_loop(long long its) {
  for(long long i = 0; i < its; ++i)
    asm("");
}

int main(int argc, char *argv[])
{
	int len = LENGTH;

	int* arr1 = init_int_arr(len);
	
	for(int i = 0; i < TIMES; ++i) {
		#pragma oss task inout(arr1 [0:len-1])
    sum_array(arr1, len);
	}

  long long int res = 0;
 
  for(int j = 0; j < 100; ++j) {
    for(int i = 0; i < TIMES; ++i) {
      #pragma oss task in(arr1[0:len-1]) reduction(+:res)
      for(size_t k = 0; k < len; ++k) {
        res += arr1[k];
      }
      busy_loop(1000000000L);
    }
    #pragma oss task inout(res)
    busy_loop(1000000000L);
  }

#pragma oss taskwait
  #pragma oss task in(res)
  {
    test_result("reduction_hpc", 1, res == 100);
  }

#pragma oss taskwait

	free(arr1);

	return 0;
}
