/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LENGTH 4096

void sum_array(int* arr, size_t length) {
	for(size_t i = 0; i < length; ++i) {
		arr[i]++;
	}
}

int * init_int_arr(size_t length) {
	int * res = malloc(length * sizeof(int));
	memset(res, 0, length * sizeof(int));

	return res;
}

int check_array_is(int* arr, size_t length, int val) {
	for(size_t i = 0; i < length; ++i) {
		if(arr[i] != val)
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int len = LENGTH;

	int* arr1 = init_int_arr(LENGTH);
	int* arr2 = init_int_arr(LENGTH);
	
	#pragma oss task inout(arr1 [0:len], arr2)
	sum_array(arr1, LENGTH);

	#pragma oss task inout(arr2 [0:len])
	sum_array(arr2, LENGTH);

	#pragma oss taskwait

	if(check_array_is(arr1, LENGTH, 1) && check_array_is(arr2, LENGTH, 1))
		printf("tasks_no_overlapping test 1: \033[1m\033[32mOK\033[0m\n");
	else
		printf("tasks_no_overlapping test 1: \033[1m\033[31mERROR\033[0m\n");

	free(arr1);
	free(arr2);

	return 0;
}
