#include "../test.h"

int main() {
    int i;

    int sum = 0;
    #pragma oss taskloop reduction(+: sum)
    for (i = 0; i < 10; ++i) {
        sum++;
    } 
    #pragma oss taskwait

    test_result("taskfor", 3, sum == 10);
}

