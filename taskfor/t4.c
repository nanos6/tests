#include "../test.h"
#include <stdio.h>

#define N_SUM 100
#define N 1000

int testArr[N_SUM] = {0};

// t1.c
int main() {
    int i;
    int test = 0;

    #pragma oss taskloop for reduction(+: test)
    for (i = 0; i < N; ++i) {
        test++;
    }
        

    #pragma oss taskloop for reduction(+: [N_SUM]testArr)
    for (i = 0; i < N; ++i) {
        for (int j = 0; j < 10; ++j)
            testArr[j] += j;
    }
    #pragma oss taskwait

    test_result("taskfor", 5, testArr[3] == N*3);
}

