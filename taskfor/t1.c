#include "../test.h"
#include <stdio.h>

// t1.c
int main() {
    int i;
    int sum = 0;
    #pragma oss task for reduction(+: sum)
    for (i = 0; i < 10; ++i) {
        sum++;
    }
    #pragma oss taskwait

    test_result("taskfor", 2, sum == 10);
}

