/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  int b, c, d, e, f, g;

  for(int j = 0; j < 5; ++j) {
    #pragma oss task for chunksize(512) out(a)
    for(int i = 0; i < 512; ++i) {
      __sync_fetch_and_add(&a, 1);
    }
  }
#pragma oss taskwait

  __sync_synchronize();

  printf("%d\n", a);

  if(a == 512*5)
    test_ok("taskfor", 1);
  else
    test_die("taskfor", 1);

	return 0;
}
