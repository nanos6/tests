/* test.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LENGTH 1024
#define TIMES 10

void sum_array(int* arr, size_t length, int from) {
	for(size_t i = from; i < length; ++i) {
		arr[i]++;
	}
}

int * init_int_arr(size_t length) {
	int * res = malloc(length * sizeof(int));
	memset(res, 0, length * sizeof(int));

	return res;
}

int check_array_is_asc(int* arr, size_t length, int times) {
	for(size_t i = 0; i < length; ++i) {
		if(arr[i] != ((i+1)*times))
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int len = LENGTH;

	int* arr1 = init_int_arr(LENGTH);
	int* arr2 = init_int_arr(LENGTH);
	
	for(int i = 0; i < TIMES; ++i) {
		for(int j = 0; j < LENGTH; ++j) {
			#pragma oss task inout(arr1 [j:len]) if(0)
			sum_array(arr1, LENGTH, j);
			#pragma oss task inout(arr2 [j:len]) if(0)
			sum_array(arr2, LENGTH, j);
		}
	}

	#pragma oss taskwait

	if(check_array_is_asc(arr1, LENGTH, TIMES) && check_array_is_asc(arr2, LENGTH, TIMES))
		printf("tasks_partial_overlap test 1: \033[1m\033[32mOK\033[0m\n");
	else
		printf("tasks_partial_overlap test 1: \033[1m\033[31mERROR\033[0m\n");

	free(arr1);
	free(arr2);

	return 0;
}
