/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define barrier __sync_synchronize()

void busy_loop(long long its) {
  printf("Loop start \n");
  for(long long i = 0; i < its; ++i)
    asm("");
  printf("Loop end \n");
}

int main(int argc, char *argv[])
{
  __attribute__((unused)) const int N = 16;

	int A[N][N];
  
  volatile int check = 0;
  volatile int check2 = 0;


  #pragma oss task inout(A[0;N][0;(N/2)]) shared(check, check2)
  {
    busy_loop(1000000000L);
  
    if(check == 0 && check2 == 1)
	    printf("lvalue: \033[1m\033[32mOK\033[0m (discrete-simple)\n");
    else if(check == 0 && check2 == 0)
	    printf("lvalue: \033[1m\033[32mOK\033[0m (linear-regions-fragmented)\n");
    else
	    printf("lvalue: \033[1m\033[31mERROR\033[0m\n");
  }

  #pragma oss task inout(A[0][0]) shared(check)
  {
    check = 1;
    barrier;
  }

  #pragma oss task inout(A[1][0]) shared(check2)
  {
    check2 = 1;
    barrier;
  }
	#pragma oss taskwait

	return 0;
}
