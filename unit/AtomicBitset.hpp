/*
	This file is part of Nanos6 and is licensed under the terms contained in the COPYING file.

	Copyright (C) 2020 Barcelona Supercomputing Center (BSC)
*/

#ifndef ATOMIC_BITSET_HPP
#define ATOMIC_BITSET_HPP

#include <atomic>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <vector>

#define bitsizeof(t) (sizeof(t) * 8)

template <typename backingstorage_t = uint64_t>
class AtomicBitset {
	typedef std::atomic<backingstorage_t> backing_t;
	size_t _size;
	std::vector<backing_t> _storage;
    static const backingstorage_t ONE = 1;

	static inline size_t getVecSize(size_t size)
	{
		// Round up
		return (size + bitsizeof(backingstorage_t) - 1) / bitsizeof(backingstorage_t);
	}

	inline backing_t &getStorage(size_t pos)
	{
		assert(pos < _size);
		return _storage[pos / bitsizeof(backingstorage_t)];
	}

	static inline size_t getBitIndex(size_t pos)
	{
		return pos % bitsizeof(backingstorage_t);
	}

public:
	AtomicBitset(size_t size) :
		_size(size),
		_storage(getVecSize(size))
	{
        for (backing_t &elem : _storage)
            elem = 0;
	}

	inline void set(size_t pos)
	{
		backing_t &elem = getStorage(pos);
		elem.fetch_or(ONE << getBitIndex(pos), std::memory_order_relaxed);
	}

	inline void reset(size_t pos)
	{
		backing_t &elem = getStorage(pos);
		elem.fetch_and(~(ONE << getBitIndex(pos)), std::memory_order_relaxed);
	}

	// Set the first zero bit and return the index. -1 if no zero bits were found.
	inline int setFirst()
	{
		size_t currentPos = 0;

		while (currentPos < _size) {
			backing_t &elem = getStorage(currentPos);
			backingstorage_t value = elem.load(std::memory_order_relaxed);
			int firstOne = __builtin_ffsll(~value);
			while (firstOne && (currentPos + firstOne) <= _size) {
				// Found a bit to zero. Lets set it.
				backingstorage_t mask = (ONE << (firstOne - 1));
				assert(!(value & mask));
				if (elem.compare_exchange_strong(value, value | mask, std::memory_order_relaxed))
					return (currentPos + firstOne - 1);
				// Raced with another thread. Retry.
				firstOne = __builtin_ffsll(~value);
			}

			currentPos += bitsizeof(backingstorage_t);
		}

		// We found no suitable position.
		return -1;
	}

	inline bool none() const
	{
		for (size_t i = 0; i < getVecSize(_size); ++i) {
			if (_storage[i].load(std::memory_order_relaxed))
				return false;
		}

		return true;
	}
};

#endif // ATOMIC_BITSET_HPP
