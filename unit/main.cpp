#include "AtomicBitset.hpp"
#include <iostream>
#include <cassert>

int main() {
    AtomicBitset<uint32_t> elem(128);

    assert(elem.none());
    elem.set(0);
    assert(!elem.none());
    assert(elem.setFirst() == 1);
    assert(!elem.none());
    elem.reset(0);
    assert(!elem.none());
    elem.reset(1);
    assert(elem.none());

    for (int i = 0; i < 128; ++i) {
        elem.set(i);
        assert(!elem.none());
    }

    for (int i = 0; i < 128; ++i) {
        assert(!elem.none());
        elem.reset(i);
    }

    assert(elem.none());
}
