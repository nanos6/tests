#!/bin/bash

REPS=1
ASAN=0

if [ $# -gt 0 ]; then
    REPS=$1
    echo "Configured with $REPS reps"
fi

if [ $# -gt 1 ]; then
  if [ $2 == "asan" ]; then
    echo "Using ASAN."
    ASAN=1
  fi
fi

for d in */; do
    if [ $d != "fuzzer/" ]; then
        cp Makefile $d
        cd $d
        make > /dev/null
        for f in ./bld/*; do
            echo "Executing: $d -> $f"
            if [ $ASAN == 1 ]; then
              export LSAN_OPTIONS=detect_leaks=0 
              export LD_PRELOAD=libasan.so
            fi
            for i in $(seq 1 $REPS); do
                ./$f
            done
            unset LD_PRELOAD
            unset LSAN_OPTIONS
        done
        make clean &> /dev/null
        cd ..
    fi
done
