#!/bin/python

#
# Little python3 script to analyze the output produced by Nanos6 on Verbose mode and
# that tells how many tasks were OK, and in what state are the ones that didn't make it.
# Very useful to debug dependency issues.
#

import re
import sys

if (len(sys.argv) < 2):
    sys.exit(0)

translations = ["CreationStarted", "CreationDone", "ExecutionStarted", "ExecutionDone", "Destroyed"]

def setTask(li, index, val):
    if(index in li and val > li[index]):
        li[index] = val
    elif(not index in li):
        li[index] = val

def translateVal(val):
    return translations[val]

file = sys.argv[1]
fp = open(file, "r")

tasks = dict()

line = fp.readline()
while line:
    m = re.search(r"([\-\<]\-[\-\>])\s(\w+)\D*(\d+)", line)

    #print(m)

    if(m):
        args = m.groups()
        if(args[1] == "AddTask"):
            if(args[0] == "-->"):
                setTask(tasks, args[2], 0)
            elif(args[0] == "<--"):
                setTask(tasks, args[2], 1)
        elif(args[1] == "Task"):
            if(args[0] == "-->"):
                setTask(tasks, args[2], 2)
            elif(args[0] == "<--"):
                setTask(tasks, args[2], 3)
        elif(args[1] == "DestroyTask"):
                setTask(tasks, args[2], 4)
    line = fp.readline()

cnt = 0

for key, val in tasks.items():
    if(val == 4):
        cnt += 1
    else:
        print(key, translateVal(val))

print("Correct: " + str(cnt))

fp.close()