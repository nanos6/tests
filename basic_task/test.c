/* test.c */
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int x = argc;
	#pragma oss task inout(x)
	{
		x++;
	}

	#pragma oss taskwait

	if(x == (argc + 1))
		printf("basic_task test 1: \033[1m\033[32mOK\033[0m\n");
	else
		printf("basic_task test 1: \033[1m\033[31mERROR\033[0m\n");

	return 0;
}
