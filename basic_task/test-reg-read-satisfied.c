/* test.c */
#include <stdio.h>
#include <assert.h>

int main(int argc, char *argv[])
{
	int x = argc;
	#pragma oss task inout(x)
	{
		x++;
	}

  for(int i = 0; i < 500; ++i) {
    #pragma oss task in(x) shared(argc)
    {
      if(x != (argc + 1))
        printf("Error\n");
      __attribute__((unused)) volatile int b = x;
    }
  }

	#pragma oss taskwait

	if(x == (argc + 1))
		printf("basic_task test_reg_read: \033[1m\033[32mOK\033[0m\n");
	else
		printf("basic_task test_reg_read: \033[1m\033[31mERROR\033[0m\n");

	return 0;
}
