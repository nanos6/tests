/* test.c */
#include <stdio.h>
#include <assert.h>

#define NUM_TASKS 2

int main(int argc, char *argv[])
{
    int x = 0;

    for(int i = 0; i < NUM_TASKS; ++i) {
        #pragma oss task inout(x)
        {
            x++;
        }
    }

	#pragma oss taskwait

    if(x == NUM_TASKS)
		printf("basic_task test 4: \033[1m\033[32mOK\033[0m\n");
	else
		printf("basic_task test 4: \033[1m\033[31mERROR\033[0m\n");

	return 0;
}
