/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int x = argc;
#pragma oss task inout(x)
	{
		x++;
	}

#pragma oss task in(x)
	{
		__attribute__((unused)) volatile int b = x;
		sleep(1);
	}

#pragma oss task in(x)
	{
		__attribute__((unused)) volatile int b = x;
		sleep(1);
	}

#pragma oss taskwait

	if (x == (argc + 1))
		printf("basic_task 3: \033[1m\033[32mOK\033[0m\n");
	else
		printf("basic_task 3: \033[1m\033[31mERROR\033[0m\n");

	return 0;
}
