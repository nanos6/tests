/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define REPS 500000
#define SIZE 8192

void inc_array(int* array, size_t size, int amount) {
  for(int i = 0; i < size; ++i)
    array[i] += amount;
}

int check_array(int* array, size_t size, int expected) {
  for(int i = 0; i < size; ++i) {
    if(array[i] != expected) {
      printf("Error: Position %d, Expected %d, Actual %d\n", i, expected, array[i]);
      return 0;
    }
  }

  return 1;
}

int main(int argc, char *argv[])
{
  int * array = calloc(SIZE, sizeof(int));

  for(int i = 0; i < REPS; ++i) {
    #pragma oss task reduction(+: [8192]array)
    inc_array(array, SIZE, 1);
  }

	#pragma oss taskwait

	if(check_array(array, SIZE, REPS))
		printf("array_reduction test 1: \033[1m\033[32mOK\033[0m\n");
	else
		printf("array_reduction test 1: \033[1m\033[31mERROR\033[0m\n");

  free(array);

	return 0;
}
