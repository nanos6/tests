/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;

  #pragma oss task inout(a)
  {
    #pragma oss task concurrent(a)
    {
      sleep(1);
      if (a != 1)
        test_die("commutative_concurrent", 1);
    }
  }

  #pragma oss task concurrent(a)
  a = 1;

	#pragma oss taskwait

  test_ok("commutative_concurrent", 1);

	return 0;
}
