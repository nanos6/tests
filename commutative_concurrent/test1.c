/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  int b = 0;

  #pragma oss task inout(a) inout(b)
  {
    #pragma oss task inout(b)
    sleep(1);

    #pragma oss task commutative(a) inout(b)
    {
      sleep(1);
      if (a != 1)
        test_die("commutative_concurrent", 2);
    }
  }

  #pragma oss task commutative(a)
  a = 1;

	#pragma oss taskwait

  test_ok("commutative_concurrent", 2);

	return 0;
}
