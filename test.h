#include <stdio.h>
#include <stdlib.h>

#define _unused __attribute__((unused))

void test_ok(const char * name, int num) {
    printf("%s test %d: \033[1m\033[32mOK\033[0m\n", name, num);        
}

void test_die(const char * name, int num) {
    printf("%s test %d: \033[1m\033[31mERROR\033[0m\n", name, num);
    exit(1);
}

void test_die_verbose(const char * name, int num, const char * info) {
    printf("%s\n", info);
    test_die(name, num);
}

void test_result(const char * name, int num, int result) {
    if(result)
        test_ok(name, num);
    else
        test_die(name, num);
}
