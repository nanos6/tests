/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;

  for(int i = 0; i < 100; ++i) {
    #pragma oss task weakin(a)
    {
      for(int j = 0; j < 1000; ++j) {
        #pragma oss task in(a)
        {
          _unused volatile int c = a;
        }
      }
    }
  }

	#pragma oss taskwait

  test_ok("weak_early", 3);

	return 0;
}
