/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task weakinout(a) shared(sync)
  {
    #pragma oss task in(a) shared(sync)
    {
      sleep(1);
      if(sync != 0)
        test_die_verbose("weak_early", 8, "first in");
      sync = 1;
    }

    #pragma oss task inout(a) shared(sync)
    {
      if(sync != 1)
        test_die_verbose("weak_early", 8, "inout");
      sync = 2;
      a = 1;
    }

    #pragma oss task in(a) shared(sync)
    {
      sleep(1);
      if(sync != 3 || a != 1)
        test_die_verbose("weak_early", 8, "second in");
    }
  }

  #pragma oss task in(a) shared(sync)
  {
    if(sync != 2 || a != 1) {
      printf("%d | %d\n", sync, a);
      test_die_verbose("weak_early", 8, "third in");
    }
    sync = 3;
  }

	#pragma oss taskwait

  test_ok("weak_early", 8);

	return 0;
}
