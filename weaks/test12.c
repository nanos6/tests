/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task weakinout(a) shared(sync)
  {
    #pragma oss task in(a) shared(sync)
    {
      sync = 1;
    }

    #pragma oss task weakout(a) shared(sync)
    {
        #pragma oss task out(a) shared(sync)
        {
            sleep(1);
            if(sync != 1)
              test_die("weak_early", 13);
            sync = 2;
        }
    }

    #pragma oss task weakin(a) shared(sync)
    {
      #pragma oss task in(a) shared(sync)
      {
        if(sync != 2)
          test_die("weak_early", 13);
      }
    }
  }
  

	#pragma oss taskwait

  test_ok("weak_early", 13);

	return 0;
}
