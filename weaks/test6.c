/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task inout(a) shared(sync)
  {
    a = 1;

    #pragma oss task in(a) shared(sync)
    {
      sleep(1);
      if(sync != 1 || a != 1)
        test_die("weak_early", 7);
    }
  }

  #pragma oss task in(a) shared(sync)
  {
    if(a != 1 || sync != 0)
      test_die("weak_early", 7);
    sync = 1;
    __sync_synchronize();
  }

	#pragma oss taskwait

  test_ok("weak_early", 7);

	return 0;
}
