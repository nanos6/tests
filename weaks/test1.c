/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  int b = 0;

  #pragma oss task weakinout(a) weakinout(b)
  {
    #pragma oss task inout(a) inout(b)
    {
      sleep(1);
      a = 1;
    }

    #pragma oss taskwait

    #pragma oss task inout(a) inout(b)
    {
      sleep(1);
      a++;
    }

    #pragma oss task inout(b)
    {
      sleep(1);
      b = 1;
    }
  }

  #pragma oss task weakinout(a) shared(b)
  {
    #pragma oss task inout(a) shared(b)
    {
      // This should execute after the first task but before the second one finishes (early release).
      printf("RES: a -> %d | b -> %d\n", a, b);
      test_result("weak_early", 2, (a == 2 && b == 0));
    }
  }

	#pragma oss taskwait

	return 0;
}
