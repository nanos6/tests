/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task weakinout(a) shared(sync)
  {
    #pragma oss task out(a) shared(sync)
    {
      sleep(1);
      if(sync != 0)
        test_die("weak_early", 9);
      a = 1;
    }

    #pragma oss task in(a) shared(sync)
    {
      sleep(1);
      if(sync != 1 || a != 1)
        test_die("weak_early", 9);
    }
  }

  #pragma oss task in(a) shared(sync)
  {
    if(sync != 0 || a != 1)
        test_die("weak_early", 9);
      sync = 1;
  }

	#pragma oss taskwait

  test_ok("weak_early", 9);

	return 0;
}
