/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;

  #pragma oss task out(a)
  {
    sleep(1);
    int r = __sync_fetch_and_add(&a, 1);
    if(r != 0)
      test_die("weak_early", 14);
  }

  #pragma oss task weakout(a)
  {
    #pragma oss task out(a)
    {
      sleep(1);
      int r = __sync_fetch_and_add(&a, 1);
      if(r != 1)
        test_die("weak_early", 14);
    }
  }

  #pragma oss task out(a)
  {
    int r = __sync_fetch_and_add(&a, 1);
    if(r != 2)
      test_die("weak_early", 14);
  }

	#pragma oss taskwait

  test_ok("weak_early", 14);

	return 0;
}
