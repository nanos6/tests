/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task weakin(a) shared(sync)
  {
      sync = 1;
      #pragma oss task in(a) shared(sync)
      {
          sync = 1;
      }
  }

  #pragma oss task weakout(a) shared(sync)
  {
      sync = 1;
      #pragma oss task out(a) shared(sync)
      {

      }
  }

	#pragma oss taskwait

  test_ok("weak_early", 11);

	return 0;
}
