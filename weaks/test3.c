/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  int b = 0;

  for(int i = 0; i < 100; ++i) {
    #pragma oss task weakinout(a) weakin(b)
    {
      for(int j = 0; j < 1000; ++j) {
        #pragma oss task inout(a) in(b)
        {
          _unused volatile int c = a;
        }
      }
    }
  }

	#pragma oss taskwait

  test_ok("weak_early", 4);

	return 0;
}
