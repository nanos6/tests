/* test.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include "../test.h"

int main(int argc, char *argv[])
{
  int a = 0;
  volatile int sync = 0;

  #pragma oss task weakinout(a) shared(sync)
  {
    #pragma oss task inout(a)
    {
      a = 1;
    }

    #pragma oss task in(a) shared(sync)
    {
      if(a != 1)
        test_die_verbose("weak_early", 6, "in before inout!");
      sleep(2);
      if(sync != 1)
        test_die_verbose("weak_early", 6, "sync != 1 -> no early release");
    }

    sleep(1);
  }

  #pragma oss task weakin(a) shared(sync)
  {
    #pragma oss task in(a) shared(sync)
    {
      if(sync != 0)
        test_die("weak_early", 6);

      sync = 1;
      __sync_synchronize();
    }
  }

	#pragma oss taskwait

  test_ok("weak_early", 6);

	return 0;
}
